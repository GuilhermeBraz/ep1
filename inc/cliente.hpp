#ifndef CLIENTE_HPP
#define CLIENTE_HPP

#include "usuario.hpp"
#include <string>


using namespace std;

class Cliente : public Usuario{
private:
    string data_de_cadastro;
    int total_de_compras;
public:
    // Métodos
    Cliente();  // Construtor
    Cliente(string nome, long int cpf);
    Cliente(string nome, long int cpf, string telefone, string email, string data_de_cadastro,
    int total_de_compras,bool socio);
    ~Cliente(); // Destruror
    // Métodos acessores
    void set_data_de_cadastro(string data_de_cadastro);
    string get_data_de_cadastro();
    void set_total_de_compras(int total_de_compras);
    int get_total_de_compras();
    // Outros Métodos
    void imprime_dados();

};

#endif
