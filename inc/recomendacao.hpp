/*
 * recomendacao.hpp
 *
 *  Created on: 1 de out de 2019
 *      Author: braz
 */

#ifndef RECOMENDACAO_HPP
#define RECOMENDACAO_HPP
#include<string>
#include"produto.hpp"
#include"carrinho.hpp"
#include<vector>

class Recomendacao : public Carrinho {
public:
	Recomendacao();
	~Recomendacao();
	void imprimir_recomendacoes(string email);
};



#endif
