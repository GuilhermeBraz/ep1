/*
 * cadastro.hpp
 *
 *  Created on: 23 de set de 2019
 *      Author: braz
 */

#ifndef CADASTRO_HPP
#define CADASTRO_HPP
#include <string>
#include<vector>

class Usuario;
class Cliente;


class Cadastro{
public:
	typedef struct itens{
		std::string nome;
		long int cpf;
		std::string telefone;
		std::string email;
		std::string data_de_cadastro;
		float total_de_compras;
		bool socio;
	}ITENS;
	ITENS reg;
	//std::vector<Usuario*>usuario;
	//std::vector<Cliente*>cliente;
	//std::vector<Admin*>admin;
	//funcoes
	Cadastro();
	virtual ~Cadastro();
	template <typename T1>
	T1 getInput();
	std::string getString();
//cadastro cliente e login
	void registro();
	bool login();
//modo estoque
	void registroProduto();
	void registroCategoria();
	virtual void editProduto();
	void atualizaCliente(std::string email,int quantidade);

};



#endif
