#ifndef USUARIO_HPP
#define USUARIO_HPP

#include <string>

using namespace std;

class Usuario{
//    protected:
    private:
        string nome;
        long int cpf;
        string telefone;
        string email;
        //if socio == true 15% de desconto
        bool socio;
    public:
        Usuario();
        ~Usuario();
        string get_nome();
        void set_nome(string nome);
        long int get_cpf();
        void set_cpf(long int cpf);
        string get_telefone();
        void set_telefone(string telefone);
        string get_email();
        void set_email(string email);
        void set_socio(bool socio);
        bool get_socio();
        virtual void imprime_dados();
};

#endif











