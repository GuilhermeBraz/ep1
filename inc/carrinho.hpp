/*
 * carrinho.hpp
 *
 *  Created on: 29 de set de 2019
 *      Author: braz
 */

#ifndef CARRINHO_HPP
#define CARRINHO_HPP

#include<string>
#include"produto.hpp"
#include"cadastro.hpp"
#include<vector>

class Carrinho: public Cadastro{
private:
	bool verificarSocio;
	int quantProdutos;
	float valorTotal;
	float desconto;
public:
	vector<Produto*>produto;
	Carrinho();
	Carrinho(bool verificar);
	virtual ~Carrinho();
	//getters e setters
	const vector<Produto*>& getProduto() const;
	void setProduto(const vector<Produto*> &produto);
	int getQuantProdutos() const;
	void setQuantProdutos(int quantProdutos);
	bool isVerificarSocio() const;
	void setVerificarSocio(bool verificarSocio);
	float getDesconto() const;
	void setDesconto(float desconto);
	float getValorTotal() const;
	void setValorTotal(float valorTotal);
	//calculos e outros metodos
	void calculaDesconto(bool verificar);
	void addProduto();
	//sobreescrita
	void editProduto(std::string codigo,std::string categoria,int quantidadeEtoque,bool operacao);
	void removerCarrinho();
	//usar no deslogar
	void removerCarrinhoN();
	void imprimir_dados();
	void finalizarCompra(string email,int total_compras);
};





#endif
