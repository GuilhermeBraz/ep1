#ifndef PRODUTO_HPP
#define PRODUTO_HPP

#include<string>
using namespace std;

class Produto
{
private:
	string nome;
	string categoria;
	string codigo;
	string cor;
	int quantidade_estoque;
	float preco;
	float peso;
	float tamanho;

public:
	Produto();
	Produto(string nome,string categoria,string codigo,string cor,int quantidade_estoque,float preco,float peso,float tamanho);
	~Produto();
	string get_nome();
	void set_nome(string nome);
	string get_categoria();
	void set_categoria(string categoria);
	string get_codigo();
	void set_codigo(string codigo);
	string get_cor();
	void set_cor(string cor);
	int get_quantidade_estoque();
	void set_quantidade_estoque(int quantidadeEstoque);
	float get_preco();
	void set_preco(float preco);
	float get_peso();
	void set_peso(float peso);
	float get_tamanho();
	void set_tamanho(float tamanho);
	void imprime_dados();


};



#endif
