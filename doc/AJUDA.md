
# Instruções de execução

### Tela menu

A tela de login consiste num menu como mostra abaixo:

	--------------------------------Menu----------------------------------

	(1)Modo venda
						
	(2)Modo estoque
						
	(3)Modo recomendacao
					   
	< (4)deslogar  >
						
	(0)Sair
						
	:

As funções só podem ser executadas com o login que se mantem até o usuario sair ou deslogar - quando você loga pela primeira vez ao voltar no menu uma opção deslogar irá aparecer caso queira trocar o login - caso não for possível realizar o login sera perguntado se deseja cadastrar ou não um usuario.

### login

O login do cliente é feito por apenas seu **email**

### cadastro

No cadastro será perguntado **nome, cpf, telefone, email , data de cadastro, total de compras e se é socio respectivamente**.E do mesmo jeito são gravados respectivamente em uma file .txt de nome "clientes"  em uma mais enchuta com nome "usuarios":

**clientes.txt**--exemplo com 2 clientes cadastrados

pedro  <-nome

09112309401 <-cpf

99763210 <-telefone

pedro@gmail.com <-email

21/20/2018 <- data de cadastro

3	<-total de compras

1   <- 1 quer dizer que é sócio e 0 que não é(variavel do tipo bool)

	<-separados por um \n


joao

12367849012

98769020

joao@gmail.coom

10/03/2010

10

0



**usuarios.txt**--o mesmo so que no usuarios

pedro

09112309401

pedro@gmail.com 

	<-separados por \n

joao

12367849012

joao@gmail.com



-No cadastro não pode haver nem cpf e email iguais o resto pode repetir

### Modo venda

No codigo aparece:

---------------------------Modo Venda---------------------------------

	(1)Adicionar produto

	(2)ver carrinho

	(3)finalizar compra

	(4)remover produto carrinho

	(0)voltar menu

	:


### Modo estoque

No codigo aparece:

	------------------------------Modo Estoque----------------------------

	(1)Cadastrar categoria
						
	(2)Cadastrar Produto
						
	(3)Atualizar produto
						
	(0)Voltar menu
						
	:


### Modo recomendação

	------------------------------Modo Recomendação----------------------------

	< aki ele printa os produto recomendados>


### Organização do código
codigo composto de 5 classes : **cadastro,carrinho,recomendacao,cliente,usuario**

Herança entre classes:
carrinho herda cadastro
cliente herda usuario
recomendacao herda carrinho 

## Bibliotecas utilizadas
* **iostream-basica;**
* **fstream-manipular arquivos c++;**
* **vector-usar vector de c++;**
* **cstring-usar funções para manipular string como vetor de char(char[]).Exemplo:strcat,strcpy;**
* **algorithm-usar função para ordenação do vector.Exemplo sort;**


