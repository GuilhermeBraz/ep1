#include <iostream>
//manipular arquivos
#include<fstream>
#include<vector>
#include<string>
#include "cadastro.hpp"
#include"cliente.hpp"
#include"produto.hpp"
#include "carrinho.hpp"
#include"recomendacao.hpp"

using namespace std;

string get_string(){
    string valor;
    getline(cin, valor);
    return valor;
}

template <typename T1>

T1 get_input(){
    while(true){
    T1 valor;
    cin >> valor;
    if(cin.fail()){
        cin.clear();
        //buffer
        cin.ignore(32767,'\n');
        cout << "Entrada inválida! Insira novamente: " << endl;
    }
    else{
        cin.ignore(32767,'\n');
        return valor;
    }
    }
}

int main(){

	int escolha = -1;
	bool log = false;
	//int qual = 0;
	vector<Usuario*>usuario;
	vector<Cliente*>cliente;
	vector<Produto*>produto;

	Cadastro * cadastro = new Cadastro();
	Carrinho * carrinho = new Carrinho();
	Recomendacao * recomendacao = new Recomendacao();

	while(escolha != 0)
	{
		if(log)
		{	//printar o email de quem esta logado
			cout<<"logado:"<<cadastro->reg.email<<endl;
		}
		else
		{
			cout<<"<sem login>"<<endl;
		}
		cout<<"-----------------------------Menu-----------------------------"<<endl;
		cout<<"(1)Modo Venda"<<endl;
		cout<<"(2)Modo Estoque"<<endl;
		cout<<"(3)Modo recomendacao"<<endl;
		if(log)
		{
			cout<<"(4)Deslogar"<<endl;
		}
		cout<<"(0)Sair"<<endl;
		cout<<":"<<endl;
		escolha = get_input<int>();
		//escolha menu
		switch(escolha)
		{

			case 1:
			//ver login
				if(log)
				{
					cout<<"logado: "<<cadastro->reg.email<<endl;
				}
				else
				{
					log = cadastro->login();
				}

				if(log)
				{
					if(cadastro->reg.socio)
					{
						cout<<"socio"<<endl;
						carrinho->setVerificarSocio(true);
					}
					else
					{
						cout<<"nao e socio"<<endl;
						carrinho->setVerificarSocio(false);
					}
					int choose = -1;
					while(choose != 0)
					{
						system("clear");
						cout<<"------------------------------Modo Venda----------------------------"<<endl;
						cout<<"(1)Adicionar produtos no carrinho"<<endl;
						cout<<"(2)Ver carrinho"<<endl;
						cout<<"(3)Finalizar compra"<<endl;
						cout<<"(4)remover produtos do carrinho"<<endl;
						cout<<"(0)Voltar menu"<<endl;
						cout<<":"<<endl;
						choose = get_input<int>();

						switch(choose)
						{
							case 1:
							carrinho->addProduto();
							break;

							case 2:
							carrinho->imprimir_dados();
							getchar();
							break;

							case 3:
							carrinho->finalizarCompra(cadastro->reg.email,cadastro->reg.total_de_compras);
							break;

							case 4:
							carrinho->removerCarrinho();
							break;

							case 0:
							system("clear");
							break;

							default:
							cout<<"caracter invalido"<<endl;
						}



					}


				}
			break;

			case 2:
				if(log)
				{
					cout<<"logado:"<<cadastro->reg.email<<endl;

				}
				else
				{
					log = cadastro->login();

				//	cliente.push_back(new Cliente(cadastro->reg.nome,cadastro->reg.cpf,
				//	cadastro->reg.telefone,cadastro->reg.email,cadastro->reg.data_de_cadastro,
				//	cadastro->reg.total_de_compras,cadastro->reg.socio));

				}

				//ver de novo se logo
				if(log)
				{
					int choose = -1;
					while(choose != 0)
					{
						system("clear");
						cout<<"------------------------------Modo Estoque----------------------------"<<endl;
						cout<<"(1)Cadastrar categoria"<<endl;
						cout<<"(2)Cadastrar Produto"<<endl;
						cout<<"(3)Atualizar produto"<<endl;
						cout<<"(0)Voltar menu"<<endl;
						cout<<":"<<endl;
						choose = get_input<int>();

						switch(choose)
						{
							case 1:
							cadastro->registroCategoria();
							break;

							case 2:
							cadastro->registroProduto();
							break;

							case 3:
							cadastro->editProduto();
							break;

							case 0:
							system("clear");
							break;

							default:
							cout<<"caracter invalido"<<endl;
							getchar();
							system("clear");

						}
					}
				}
			break;

			case 3:
				//ver login
				if(log)
				{
					cout<<"logado: "<<cadastro->reg.email<<endl;
				}
				else
				{
					log = cadastro->login();
				}
				//fazer so logado
				if(log)
				{
					system("clear");
					cout<<"------------------------------Modo Recomendacao----------------------------"<<endl;
					recomendacao->imprimir_recomendacoes(cadastro->reg.email);
					system("clear");

				}
			break;


			case 0:
				system("clear");
				cout<<"<sistema encerrado>"<<endl;
			break;

			default:
			cout<<"opcao invalida"<<endl;


		}
		if(escolha == 4 && log == true)
		{
			log = false;
			carrinho->removerCarrinhoN();
			system("clear");
			//cliente.pop_back();
		}
	}

	//criar objeto cliente
	cliente.push_back(new Cliente(cadastro->reg.nome,cadastro->reg.cpf,cadastro->reg.telefone,cadastro->reg.email,cadastro->reg.data_de_cadastro,cadastro->reg.total_de_compras,cadastro->reg.socio));
	usuario.push_back(new Cliente(cadastro->reg.nome,cadastro->reg.cpf,cadastro->reg.telefone,cadastro->reg.email,cadastro->reg.data_de_cadastro,cadastro->reg.total_de_compras,cadastro->reg.socio));
	for(Cliente *c:cliente)
	{
		c->imprime_dados();
	}




	return 0;

}

