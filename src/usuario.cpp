#include "usuario.hpp"
#include <iostream>

using namespace std;

Usuario::Usuario(){
    cout << "Construtor da classe Usuario" << endl;
    set_nome("");
    set_cpf(0);
    set_telefone("");
    set_email(""); 
    cout << "";   
}
Usuario::~Usuario(){
    cout << "Destrutor da classe Usuario" << endl;
}

string Usuario::get_nome(){
    return nome;
}
void Usuario::set_nome(string nome){
    this->nome = nome;
}
long int Usuario::get_cpf(){
    return cpf;
}
void Usuario::set_cpf(long int cpf){
    this->cpf = cpf;
}
string Usuario::get_telefone(){
    return telefone;
}
void Usuario::set_telefone(string telefone){
    this->telefone = telefone;
}
string Usuario::get_email(){
    return email;
}   
void Usuario::set_email(string email){
    this->email = email;
}
bool Usuario::get_socio(){
	return socio;
}
void Usuario::set_socio(bool socio){
	this->socio=socio;
}

void Usuario::imprime_dados(){
    cout << "Nome: " << get_nome() << endl;
    cout << "CPF: " << get_cpf() << endl;
    cout << "Telefone: " << get_telefone() << endl;    
    cout << "Email: " << get_email() << endl;
} 













