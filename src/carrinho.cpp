/*
 * carrinho.cpp
 *
 *  Created on: 29 de set de 2019
 *      Author: braz
 */
#include"carrinho.hpp"
#include <iostream>
#include<fstream>
#include<vector>
#include<cstring>
#include<algorithm>

using namespace std;

//construtor e destrutor
Carrinho::Carrinho(){
	setQuantProdutos(0);
	setVerificarSocio(false);
	setDesconto(0.0f);
	setValorTotal(0.0f);
}
Carrinho::Carrinho(bool verificar) {
	setQuantProdutos(0);
	setVerificarSocio(verificar);
	setDesconto(0.0f);
	setValorTotal(0.0f);
}

Carrinho::~Carrinho() {

}

//getters e setters
const vector<Produto*>& Carrinho::getProduto() const {
	return produto;
}

void Carrinho::setProduto(const vector<Produto*> &produto) {
	this->produto = produto;
}

int Carrinho::getQuantProdutos() const {
	return quantProdutos;
}

void Carrinho::setQuantProdutos(int quantProdutos) {
	this->quantProdutos = quantProdutos;
}

bool Carrinho::isVerificarSocio() const {
	return verificarSocio;
}

float Carrinho::getDesconto() const {
	return desconto;
}

void Carrinho::setDesconto(float desconto) {
	this->desconto = desconto;
}

float Carrinho::getValorTotal() const {
	return valorTotal;
}

void Carrinho::setValorTotal(float valorTotal) {
	this->valorTotal = valorTotal;
}

void Carrinho::setVerificarSocio(bool verificarSocio) {
	this->verificarSocio = verificarSocio;
}

//metodos mais complexos
void Carrinho::calculaDesconto(bool verificar) {
	if(verificar)
	{
		setDesconto(0.15*valorTotal);
	}
	else
	{
		setDesconto(0.0f);
	}
}

//sobreescrita metodo edit produto de cadastro
void Carrinho::editProduto(string codigo,string categoria,int quantidadeEstoque,bool operacao){//operacao true - , false +
	    fstream arquivo;
		ofstream aux;

		string linha;
		char cat[30];
		int i = 0;
		int achou = 0;

		strcpy(cat,categoria.c_str());

	//	cout<<cat<<endl;
		strcat(cat,".txt");
	//	cout<<"titulo "<<cat<<endl;
		getchar();
		arquivo.open( cat , ios::in);
		aux.open("auxiliar.txt",ios::app);

		if(arquivo.is_open())
		{

			while(getline(arquivo,linha))
			{
				//codigo = linha 1
				if(codigo == linha && achou == 0)
				{
					i = 0;
					achou = 1;
				}



				//quant = linha 3
				if(i == 2 && achou == 1 )
				{
					//cout<<"antiga: "<<linha<<endl;
					//subtrair 1 quando for no modo add produto (operacao = true)
					if(operacao)
					{
						linha = to_string(quantidadeEstoque-1);
					}
					else	//voltar a antiga quando for remove do carrinho (opeeracao = false)-no vetor eu n subtraio
					{
						linha = to_string(quantidadeEstoque);
					}


				//	cout<<"atualizado: "<<linha<<endl;
				//	getchar();
				}

				aux<<linha<<endl;
					i++;

			}
			arquivo.close();
			aux.close();
			if(achou == 1)
			{
				//atualizar arquivo
				remove(cat);
				rename("auxiliar.txt",cat);
			//	cout<<"atualizado com sucesso"<<endl;
			//	getchar();
			}
			else
			{
				remove("auxiliar.txt");
				cout<<"produto nao encontrado"<<endl;
				getchar();
			}
		}
		else
		{
			cout<<"categoria nao existe"<<endl;
			getchar();
		}

}


void Carrinho::addProduto()
{
		fstream arquivo;
		ofstream aux;

		string linha;
	//	char cat[30];
		int i = 0;
		int x = 0;
		int l = 0;
		int zero = 0;
		int onde = 0;
		int achou = 0;

		//variaveis de produto
		string nome;
		string codigo;
		string categoria;
		string cor;
		int quantidade_estoque;
		float preco;
		float peso;
		float tamanho;


		cout<<"Escolha a categoria do produto:"<<endl;
		categoria = getString();
	//	scanf("%[^\n]",cat);;
	//	cout<<cat<<endl;
	//	strcat(cat,".txt");
	//	cout<<"titulo "<<cat<<endl;
	//	getchar();
		arquivo.open(categoria+".txt" , ios::in);
	//	aux.open("carrinho.txt",ios::app);

		if(arquivo.is_open())
		{


			cout<<"Digite o codigo do produto:"<<endl;
			codigo = getString();

			while(getline(arquivo,linha))
			{

			/*	cout<<"achou: "<<achou<<endl;
				cout<<"i: "<<i<<endl;
				cout<<"l: "<<l<<endl;
				cout<<"onde: "<<onde<<endl;
				cout<<"onde+x :"<<onde + x<<endl;*/

				if(i == 8)
				{
					i=0;
				}

				if(l == onde+x && achou == 1 )
				{

				//com arquivo
				//	aux<<linha<<endl;
				//	cout<<"gravando..."<<linha<<endl;
				//	getchar();

					switch(x)
					{
						case 0:
					//	cout<<linha<<"nome"<<endl;
					//	getchar();
						nome = linha;
						break;

						case 1:
					//	cout<<linha<<"codigo"<<endl;
						codigo = linha;
						break;

						case 2:
					//	cout<<linha<<"cor"<<endl;
						cor = linha;
						break;

						case 3:
					//	cout<<linha<<"stoi"<<endl;
					//	getchar();
						quantidade_estoque = stoi(linha);
						break;

						case 4:
						preco = stof(linha);
						break;

						case 5:
						peso = stof(linha);
						break;

						case 6:
						tamanho = stof(linha);
						break;

						default:
						cout<<""<<endl;
					}
					//ir para as outras posicoes
					if(x<7)
					{
						x++;
					}

				}

				//codigo = linha 1
				if(codigo == linha && i == 1 && achou == 0)
				{
					i = 0;
					achou = 1;
					onde = l-1;
					l=0;
					zero =1;
					arquivo.seekg(0);
				}
				if(zero == 0)
				{
					l++;
					i++;

				}
				zero = 0;
			}
			arquivo.close();
		//	aux.close();
			if(achou == 1)
			{
				if(quantidade_estoque > 0)
				{
					cout<<"adicionado com sucesso"<<endl;
					produto.push_back(new Produto(nome,categoria,codigo,cor,quantidade_estoque,preco,peso,tamanho));
					//produto[0]->imprime_dados();
					//reduzir quant no estoque
					editProduto(codigo,categoria,quantidade_estoque,true);
					setQuantProdutos(quantProdutos+1);
					setValorTotal(valorTotal+preco);
					cout<<"nova quant carrinho: "<<quantProdutos<<endl;
				}
				else
				{
					cout<<"produto esgotado no momento"<<endl;
					getchar();
				}
			}
			else
			{
				cout<<"produto nao encontrado"<<endl;
				getchar();
			}
		}
		else
		{
			cout<<"categoria nao existe"<<endl;
			getchar();
		}


	}


void Carrinho::imprimir_dados(){
	int i = 0;
	calculaDesconto(verificarSocio);
	for(Produto *p:produto){
		cout<<"<"<<i<<"> ";
		p->imprime_dados();
		cout<<"------------------------------------------------------------"<<endl;
		i++;
	}
	cout<<"----------------------------------------------------------------"<<endl;
	cout<<"Valor da compra: "<<getValorTotal()<<endl;
	cout<<"Valor do desconto: "<<getDesconto()<<endl;
	cout<<"----------------------------------------------------------------"<<endl;
	cout<<"Valor final: "<<getValorTotal()-getDesconto()<<endl;

}

void Carrinho::removerCarrinho(){
	int escolha,opcao;
	int i = 0;
	//chose = -1;

	if(getQuantProdutos() > 0){
				cout<<"(1)Esvaziar carrinho"<<endl;
				cout<<"(2)Remover um produto por indice"<<endl;
				cout<<":";
				opcao = getInput<int>();
				switch(opcao)
				{
					case 1:
					//atualizar quantidade antes de deletar
						for(Produto*p:produto)
						{
							editProduto(p->get_codigo(),p->get_categoria(),p->get_quantidade_estoque(),false);
						}
						setValorTotal(0.0f);
						setQuantProdutos(0);
					//	setFrete(0.0f);
						produto.clear();
						cout<<"carrinho esvazido"<<endl;
						getchar();
					break;

					case 2:
						//printar opcoes
						for(Produto *p:produto)
						{
							cout<<"<"<<i<<"> ";
							p->imprime_dados();
							cout<<"------------------------------------------------------"<<endl;
							i++;
						}
						cout<<"Escolha o produto pelo indice <0> a <"<<i-1<<">"<<endl;
						escolha = getInput<int>();
						//achar o vetor pelo indice e da erase
						i=0;
						for(Produto *p:produto)
						{
							if(escolha == i)
							{
								editProduto(p->get_codigo(),p->get_categoria(),p->get_quantidade_estoque(),false);
								setValorTotal(valorTotal-p->get_preco());
								setQuantProdutos(quantProdutos-1);
							}
							i++;
						}
						//deletar
						produto.erase(produto.begin()+escolha);
						cout<<"produto retirado"<<endl;
						getchar();
					break;

					default:
					cout<<"entrada invalida"<<endl;

				}
	}
	else
	{
		cout<<"O carrinho esta vazio "<<endl;
		getchar();
	}
}

//sobrecarga removerCarrinho
void Carrinho::removerCarrinhoN(){
		if(getQuantProdutos()>0)
		{
			//atualizar quantidade antes de deletar
			for(Produto*p:produto)
			{
				editProduto(p->get_codigo(),p->get_categoria(),p->get_quantidade_estoque(),false);
			}
			setValorTotal(0.0f);
			setQuantProdutos(0);
			produto.clear();
			cout<<"carrinho esvazido"<<endl;
			getchar();
		}

}

//finalizar compra
void Carrinho::finalizarCompra(string email,int total_compras){

	ifstream arquivoIn,arquivoIn2;
	ofstream arquivoOut;
	string linha;
//	bool nAchou = true;
	int i = 0;
	int j;


	//apagar o carrinho com os dados diminuidos
	if(getQuantProdutos()>0)
	{
		vector<int>cats;
		vector<string>categorias;
		arquivoIn.open("categorias.txt");
		if(arquivoIn.is_open())
		{
			while(getline(arquivoIn,linha))
			{
				//abrir as categorias
				arquivoIn2.open(linha+".txt");
				if(arquivoIn2.is_open())
				{
					//jogar categoria no vector de categorias
					cats.push_back(0);
					categorias.push_back(linha);
					i++;
				}

			}
			arquivoOut.open(email+"_recomendacoes.txt",ios::app);
			//adicionar em qual cat ele comprou
			for(Produto*p:produto)
			{
				for(j = 0;j<i;j++)
				{
					if(categorias[j] == p->get_categoria())
					{
						//cats[j]+=1;
						arquivoOut<<categorias[j]<<endl;
					}
				}
			}
			arquivoOut.close();
			/*sort(categorias.begin(),categorias.end());
			//abrir arquivo de recomendacoes para esse cliente
			arquivoOut.open(reg.email+"_recomendacoes.txt",ios::app);
			for(j = 0;j<i;j++)
			{
				nAchou=true;

				for(l = 0;l <i;l++ ){
					if(categorias[j] == categorias[l]){
						nAchou = false;
						cats[j]+=cats[l];
					}
				}
				if(nAchou)
				{
					arquivoOut<<categorias[j]<<endl<<endl;
				}

				arquivoOut<<to_string(cats[j])<<endl;

			}*/

			imprimir_dados();
			setValorTotal(0.0f);
			setQuantProdutos(0);
			produto.clear();
		//	cout<<"total de compras antigo: "<<total_compras<<endl;
		//	cout<<"email: "<<email<<endl;
		//	getchar();
			atualizaCliente(email,total_compras);
			//fechar arquivos
			arquivoIn.close();
			arquivoIn2.close();
			arquivoOut.close();
			cout<<"compra feita"<<endl;
			getchar();


		}
		else
		{
			cout<<"arquivo de categorias inexistente--erro do sistema"<<endl;
		}
	}
	else
	{
		cout<<"carrinho esta vazio"<<endl;
		getchar();
	}


}
