/*
 * produto.cpp
 *
 *  Created on: 20 de set de 2019
 *      Author: braz
 */
#include"produto.hpp"
#include<iostream>
#include<string>

Produto::Produto()
{
    set_nome("");
    std::cout << "";
    set_categoria("");
    set_codigo("");
    set_cor("padrao");
    set_peso(0.0f);
    set_preco(0.0f);
    set_quantidade_estoque(0);
    set_tamanho(0.0f);
    std::cout << "Construtor da classe Produto" << std::endl;
}
Produto::Produto(string nome, string categoria, string codigo, string cor,
		int quantidade_estoque, float preco, float peso, float tamanho) {

	 	set_nome(nome);
	    set_categoria(categoria);
	    set_codigo(codigo);
	    set_cor(cor);
	    set_peso(peso);
	    set_preco(preco);
	    set_quantidade_estoque(quantidade_estoque);
	    set_tamanho(tamanho);
	    std::cout << "Construtor da classe Produto - sobrecarga" << std::endl;
}

Produto::~Produto()
{
	std::cout<<"destruiu produto"<<endl;
}


string Produto:: get_categoria() {
		return categoria;
	}

	void Produto:: set_categoria( string categoria) {
		this->categoria = categoria;
	}

	 string Produto:: get_codigo(){
		return codigo;
	}

	void Produto::set_codigo( string codigo) {
		this->codigo = codigo;
	}

	string Produto:: get_cor() {
		return cor;
	}

	void Produto::set_cor(string cor) {
		this->cor = cor;
	}

 string Produto::get_nome() {
		return nome;
	}

	void Produto::set_nome( string nome) {
		this->nome = nome;
	}

	float Produto:: get_peso(){
		return peso;
	}

	void Produto:: set_peso(float peso) {
			this->peso = peso;
		}

		float Produto::get_preco()  {
			return preco;
		}

		void Produto:: set_preco(float preco) {
			this->preco = preco;
		}

		int Produto::get_quantidade_estoque() {
			return quantidade_estoque;
		}

		void Produto::set_quantidade_estoque(int quantidadeEstoque) {
			quantidade_estoque = quantidadeEstoque;
		}

		float Produto:: get_tamanho()  {
			return tamanho;
		}

		void Produto:: set_tamanho(float tamanho) {
			this->tamanho = tamanho;
		}
		void Produto::imprime_dados(){
		    cout << "Nome: "<<get_nome()<<endl;
		    cout << "Categoria: "<<get_categoria()<<endl;
		    cout << "Codigo: "<<get_codigo()<<endl;
		    cout << "Cor: "<<get_cor()<<endl;
		    cout << "Quantidade no estoque: "<<get_quantidade_estoque()<<endl;
		    cout << "Preco: "<<get_preco()<<endl;
		    cout<<"Peso: "<<get_peso()<<endl;
		    cout<<"Tamanho: "<<get_tamanho()<<endl;
		}


