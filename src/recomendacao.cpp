/*
 * recomendacao.cpp
 *
 *  Created on: 1 de out de 2019
 *      Author: braz
 */
#include"recomendacao.hpp"
#include <iostream>
#include<fstream>
#include<vector>
#include<cstring>
#include<algorithm>

using namespace std;

Recomendacao::Recomendacao() {
}

Recomendacao::~Recomendacao() {
}

void Recomendacao::imprimir_recomendacoes(string email) {
	ifstream arquivoIn;
	ifstream arquivoIn2;

	string linha;
	bool diferente = false;
	//i é quant de categorias
	int i = 0;
	int j,l;
	//manter contagem de produtos ate 10
	int p = 0;
	//saber quando acaba um produto
	int c = 0;

	//vetor para guardar as categorias
	vector<string>categorias;
	vector<string>sorted_categorias;
	//pro recomendacoes
	vector<int>quantCat;
	vector<int>sorted_quantCat;
	arquivoIn.open("categorias.txt");
	if(arquivoIn.is_open())
	{
		while(getline(arquivoIn,linha))
		{

			arquivoIn2.open(linha+".txt");
			if(arquivoIn2.is_open())
			{
				//jogar categoria no vector de categorias
				categorias.push_back(linha);
				quantCat.push_back(0);
				sorted_quantCat.push_back(0);
				i++;
			}

		}


		arquivoIn.close();
		arquivoIn2.close();

		arquivoIn2.open(email+"_recomendacoes.txt");

		if(arquivoIn2.is_open()){

			while(getline(arquivoIn2,linha))
			{
				//se a cat comprada é igual as categorias que tem
				for(j = 0; j<i; j++)
				{
					if(categorias[j]==linha){
						quantCat[j]++;
						sorted_quantCat[j]++;
					}
				}
				sort(sorted_quantCat.begin(),sorted_quantCat.end());
				for(j = 0;j<i;j++){
					cout<<j<<": "<<sorted_quantCat[j]<<endl;
				}
			}

			//ver se quants sao iguais
			for(j = 0; j<i; j++)
			{

				for(l = 0; l<i; l++)
				{
					if(quantCat[j]!=quantCat[l])
					{
						diferente = true;
					}
				}
			}

		//	cout<<"diferente: "<<diferente<<endl;
		//	getchar();


		}

		if(diferente)
		{
		//	cout<<"entrou diferente"<<endl;
		//	getchar();
			for(j = 0; j<i; j++)
			{
				for(l = 0; l<i; l++)
				{
					if(sorted_quantCat[j]==quantCat[l])
					{
					//	cout<<"sorted categoria: "<<sorted_quantCat[j]<<endl;
					//	cout<<"na ordem do vetor cat normal: "<<quantCat[l]<<endl;
					//	cout<<"qual categoria ta na posicao "<<l<<": "<<categorias[l]<<endl;
					//	getchar();
						sorted_categorias.insert(sorted_categorias.begin(),categorias[l]);
					}
				}
			}
			//imprimir na ordem d quant
			for(j = 0;j<i;j++)
			{
				c = 0;
				if(p == 10)
				{
					break;
				}

			//	cout<<sorted_categorias[j]<<endl;
				arquivoIn.open(sorted_categorias[j]+".txt");
				while(getline(arquivoIn,linha))
				{
					if(p == 10)
					{
						break;
					}
					cout<<linha<<endl;
					if(c == 7)
					{
						c = 0;
						//um produto
						p++;
					}
					c++;
				}
				//fechar
				arquivoIn.close();
			}


		}
		else
		{
			//colocar na ordem lexicografica
			sort(categorias.begin(),categorias.end());
			for(j = 0;j<i;j++)
			{
				c = 0;
				if(p == 10)
				{
					break;
				}

			//	cout<<categorias[j]<<endl;
				arquivoIn.open(categorias[j]+".txt");
				while(getline(arquivoIn,linha))
				{
					if(p == 10)
					{
						break;
					}
					cout<<linha<<endl;
					if(c == 7)
					{
						c = 0;
						//um produto
						p++;
					}
					c++;
				}
				//fechar
				arquivoIn.close();
			}
		}
		getchar();
	}
	else
	{
		cout<<"nao ha categorias registradas no sistema"<<endl;
	}

}
