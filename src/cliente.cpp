#include "cliente.hpp"
#include <iostream>

Cliente::Cliente(){
    set_nome("");
    std::cout << "";
    set_cpf(0);
    set_telefone("");
    set_email("email.com");
    set_data_de_cadastro("");
    set_total_de_compras(0);
    set_socio(false);
    set_total_de_compras(0.0f);
    std::cout << "Construtor da classe Cliente" << std::endl; 
}

Cliente::Cliente(string nome, long int cpf){
    set_nome(nome);
    set_cpf(cpf);
    set_telefone("");
    set_email("email.com");
    set_data_de_cadastro("");
    set_total_de_compras(0);
    std::cout << "Construtor da classe Cliente - Sobrecarga 1" << std::endl; 
}



Cliente::Cliente(string nome, long int cpf, string telefone, string email, string data_de_cadastro, int total_de_compras,bool socio){
    set_nome(nome);
    set_cpf(cpf);
    set_telefone(telefone);
    set_email(email);
    set_data_de_cadastro(data_de_cadastro);
    set_total_de_compras(total_de_compras);
    set_socio(socio);
    std::cout << "Construtor da classe Cliente - Sobrecarga 2" << std::endl;     
}
    

Cliente::~Cliente(){
    std::cout << "Destrutor da classe Cliente"<< std::endl;
}
void Cliente::set_data_de_cadastro(string data_de_cadastro){
    this->data_de_cadastro = data_de_cadastro;
}
string Cliente::get_data_de_cadastro(){
    return data_de_cadastro;
}

void Cliente::set_total_de_compras(int total_de_compras){
    this->total_de_compras = total_de_compras;
}
int Cliente::get_total_de_compras(){
	return total_de_compras;
}

void Cliente::imprime_dados(){
    cout << "Nome: " << get_nome() << endl;
    cout << "CPF: " << get_cpf() << endl;
    cout << "Telefone: " << get_telefone() << endl;    
    cout << "Email: " << get_email() << endl;
    cout << "Data de Cadastro: " << get_data_de_cadastro() << endl;
    cout << "Total de Compras: " << get_total_de_compras() << endl;
    cout<<"Socio "<<get_socio()<<endl;

} 






